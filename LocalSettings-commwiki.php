<?php

$wgSitename = "WMAU Committee";
$wgServer = "https://comm.wikimedia.org.au";

$wgEnableCreativeCommonsRdf = false;
$wgRightsPage = "";
$wgRightsUrl = "";
$wgRightsText = "";
$wgRightsIcon = "";

$wgGroupPermissions['*']['createaccount'] = false;
$wgGroupPermissions['*']['read'] = false;
$wgGroupPermissions['current'] = $wgGroupPermissions['user'];
$wgGroupPermissions['user'] = [];
$wgGroupPermissions['current' ]['createaccount'] = true;
$wgGroupPermissions['current']['editinterface'] = true;
$wgGroupPermissions['sysop']['userrights'] = true;

$wgWhitelistRead = [ 'Main Page', 'Special:ChangeCredentials', 'Special:ChangePassword' ];

$wgFileExtensions = [
	'png', 'gif', 'jpg', 'jpeg', 'svg', 'odt', 'pdf', 'ods', 'odp', 'odg', 'ogg', 'ogm', 'wav',
	'm4a', 'ppt', 'psd', 'mp3', 'xls', 'xlsx', 'swf', 'doc', 'docx', 'mpp', 'sla', 'gz',
];

$wgLogo = "https://wikimedia.org.au/w/images/c/c9/Logo.png";

$wgGroupPermissions['user']['edituser'] = false;
$wgGroupPermissions['sysop']['edituser'] = true;
$wgGroupPermissions['user']['edituser-exempt'] = false;
$wgGroupPermissions['sysop']['edituser-exempt'] = true;

define( "NS_MEETING", 200 );
define( "NS_MEETING_TALK", 201 );
define( "NS_RESOLUTION", 202 );
define( "NS_RESOLUTION_TALK", 203 );
define( "NS_PROPOSAL", 204 );
define( "NS_PROPOSAL_TALK", 205 );

$wgExtraNamespaces[NS_MEETING] = "Meeting";
$wgExtraNamespaces[NS_MEETING_TALK] = "Meeting_talk";
$wgExtraNamespaces[NS_RESOLUTION] = "Resolution";
$wgExtraNamespaces[NS_RESOLUTION_TALK] = "Resolution_talk";
$wgExtraNamespaces[NS_PROPOSAL] = "Proposal";
$wgExtraNamespaces[NS_PROPOSAL_TALK] = "Proposal_talk";

$wgContentNamespaces = [
	NS_MAIN,
	NS_MEETING,
	NS_RESOLUTION,
	NS_PROPOSAL,
];

$wgNamespacesToBeSearchedDefault = [
	NS_MAIN =>           true,
	NS_TALK =>           true,
	NS_USER =>           true,
	NS_USER_TALK =>      true,
	NS_PROJECT =>        true,
	NS_PROJECT_TALK =>   true,
	NS_FILE =>          false,
	NS_FILE_TALK =>     false,
	NS_MEDIAWIKI =>      false,
	NS_MEDIAWIKI_TALK => false,
	NS_TEMPLATE =>       false,
	NS_TEMPLATE_TALK =>  false,
	NS_HELP =>           false,
	NS_HELP_TALK =>      false,
	NS_CATEGORY =>       true,
	NS_CATEGORY_TALK =>  false,
	NS_MEETING =>        true,
	NS_MEETING_TALK =>   true,
	NS_RESOLUTION =>     true,
	NS_RESOLUTION_TALK => true,
	NS_PROPOSAL =>       true,
	NS_PROPOSAL =>       true,
];

$wgNamespacesWithSubpages = [
	NS_MAIN           => true,
	NS_TALK           => true,
	NS_USER           => true,
	NS_USER_TALK      => true,
	NS_PROJECT        => true,
	NS_PROJECT_TALK   => true,
	NS_FILE_TALK      => true,
	NS_MEDIAWIKI_TALK => true,
	NS_TEMPLATE_TALK  => true,
	NS_HELP_TALK      => true,
	NS_CATEGORY_TALK  => true,
	NS_MEETING        => true,
	NS_MEETING_TALK   => true,
	NS_RESOLUTION     => true,
	NS_RESOLUTION_TALK => true,
];

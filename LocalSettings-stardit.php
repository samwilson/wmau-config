<?php

$wgSitename = "STARDIT";
$wgServer = "https://stardit.wikimedia.org.au";

$wgLocalInterwiki = $wgSitename;

$wgLogos = [
	'1x' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Standardised_Data_on_Initiatives_%28STARDIT%29_Beta_Version_Logo_0.2_%28no_text%29.svg/82px-Standardised_Data_on_Initiatives_%28STARDIT%29_Beta_Version_Logo_0.2_%28no_text%29.svg.png',
];
$wgFavicon = $wgLogos['1x'];

$wgEnableUploads = false;

$wgEnableCreativeCommonsRdf = true;

$wgGroupPermissions['*']['edit'] = false;
$wgGroupPermissions['user']['edit'] = true;

$wgPluggableAuth_EnableLocalLogin = false; 

wfLoadExtension( 'Matomo' );
$wgMatomoURL = 'stats.wikimedia.org.au';
$wgMatomoIDSite = '4';

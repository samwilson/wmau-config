<?php

$wgSitename = "Wikimedia Australia";
$wgServer = "https://wikimedia.org.au";

$wgDefaultSkin = 'WMAU';

$wgGroupPermissions['*']['edit'] = false;
$wgGroupPermissions['*']['createaccount'] = true;
$wgGroupPermissions['financial'] = $wgGroupPermissions['user'];
$wgGroupPermissions['financial']['edit'] = true;
$wgGroupPermissions['sysop']['userrights']  = true;

define("NS_MEETING", 200);
define("NS_MEETING_TALK", 201);
define("NS_RESOLUTION", 202);
define("NS_RESOLUTION_TALK", 203);
define("NS_PROPOSAL", 204);
define("NS_PROPOSAL_TALK", 205);
 
$wgExtraNamespaces[NS_MEETING] = "Meeting";
$wgExtraNamespaces[NS_MEETING_TALK] = "Meeting_talk";
$wgExtraNamespaces[NS_RESOLUTION] = "Resolution";
$wgExtraNamespaces[NS_RESOLUTION_TALK] = "Resolution_talk";
$wgExtraNamespaces[NS_PROPOSAL] = "Proposal";
$wgExtraNamespaces[NS_PROPOSAL_TALK] = "Proposal_talk";

$wgNamespaceProtection[NS_MAIN] = $wgNamespaceProtection[NS_PROJECT] =
$wgNamespaceProtection[NS_FILE] = $wgNamespaceProtection[NS_MEDIAWIKI] = $wgNamespaceProtection[NS_TEMPLATE] =
$wgNamespaceProtection[NS_HELP] = $wgNamespaceProtection[NS_CATEGORY] = 
$wgNamespaceProtection[NS_MEETING] = $wgNamespaceProtection[NS_RESOLUTION] = $wgNamespaceProtection[NS_PROPOSAL] =
	array( 'edit-main' );

$wgGroupPermissions['financial']['edit-main'] = true;

$wgFileExtensions = [ 'png', 'gif', 'jpg', 'jpeg','svg','pdf','ogg' ];

$wgContentNamespaces = [
	NS_MAIN,
	NS_MEETING,
	NS_RESOLUTION,
	NS_PROPOSAL,
];

$wgNamespacesToBeSearchedDefault = [
	NS_MAIN =>           true,
	NS_TALK =>           false,
	NS_USER =>           false,
	NS_USER_TALK =>      false,
	NS_PROJECT =>        true,
	NS_PROJECT_TALK =>   false,
	NS_FILE =>           false,
	NS_FILE_TALK =>      false,
	NS_MEDIAWIKI =>      false,
	NS_MEDIAWIKI_TALK => false,
	NS_TEMPLATE =>       false,
	NS_TEMPLATE_TALK =>  false,
	NS_HELP =>           false,
	NS_HELP_TALK =>      false,
	NS_CATEGORY =>       false,
	NS_CATEGORY_TALK =>  false,
	NS_MEETING =>        true,
	NS_MEETING_TALK =>   false,
	NS_RESOLUTION =>     true,
	NS_RESOLUTION_TALK => false,
	NS_PROPOSAL => true,
	NS_PROPOSAL_TALK => false,
];

$wgNamespacesWithSubpages = [
	NS_MAIN           => true,
	NS_TALK           => true,
	NS_USER           => true,
	NS_USER_TALK      => true,
	NS_PROJECT        => true,
	NS_PROJECT_TALK   => true,
	NS_FILE_TALK      => true,
	NS_MEDIAWIKI_TALK => true,
	NS_TEMPLATE_TALK  => true,
	NS_HELP_TALK      => true,
	NS_CATEGORY_TALK  => true,
	NS_MEETING        => true,
	NS_MEETING_TALK   => true,
	NS_RESOLUTION     => true,
	NS_RESOLUTION_TALK => true,
	NS_PROPOSAL => true,
	NS_PROPOSAL => true,
];

$wgLogo = "{$wgUploadPath}/c/c9/Logo.png";

$wgImportSources = ['commwiki', 'meta' ];

wfLoadExtension( 'Poem' );

wfLoadExtensions( [ 'ConfirmEdit', 'ConfirmEdit/QuestyCaptcha' ] );

$wgOverrideSiteFeed['atom'] = 'https://wikimedia.org.au/news.xml';

wfLoadExtension( 'Matomo' );
$wgMatomoURL = 'stats.wikimedia.org.au';
$wgMatomoIDSite = '1';

# wmau-config

This repository contains the configuration files and documentation
for the [Wikimedia Australia](https://wikimedia.org.au) wikis.

See https://wikimedia.org.au/wiki/Tech for more details.

* This repo is checked out to `/srv/wmau-config/`
* MediaWiki installed from tarball at `/var/www/mediawiki/` for all sites.

## Installation

```console
sudo apt install \
    apache2 certbot python3-certbot-apache \
    php php-mbstring php-xml php-curl php-mysql php-gd php-apcu \
    mariadb-server git composer geoip-bin geoip-database exim4
```

Download MediaWiki tarball to `/var/www/mediawiki/`, then create symlinks (see below).

Disable the default sites, and enable the Apache sites with:

```console
sudo a2dissite *
sudo a2ensite mediawiki matomo wikipedia
```

## Updating

To update this repo:

```console
cd /srv/wmau-config/
sudo git pull
```

## Symlinks

The following symlinks should be set up when deploying this repo to a new server.

```console
ln -s /srv/wmau-config/systemd/mediawiki-jobs-commwiki.service /etc/systemd/system/mediawiki-jobs-commwiki.service
ln -s /srv/wmau-config/systemd/mediawiki-jobs-wmauwiki.service /etc/systemd/system/mediawiki-jobs-wmauwiki.service
ln -s /srv/wmau-config/systemd/mediawiki-jobs-stardit.service /etc/systemd/system/mediawiki-jobs-stardit.service

ln -s /srv/wmau-config/composer.local.json /var/www/mediawiki/composer.local.json

ln -s /srv/wmau-config/apache/mediawiki.conf /etc/apache2/sites-available/mediawiki.conf
ln -s /srv/wmau-config/apache/matomo.conf /etc/apache2/sites-available/matomo.conf
ln -s /srv/wmau-config/apache/wikipedia.conf /etc/apache2/sites-available/wikipedia.conf

ln -s /srv/wmau-config/LocalSettings.php /var/www/mediawiki/LocalSettings.php

ln -s /srv/wmau-config/robotstxt /var/www/mediawiki/robotstxt
```

## Private LocalSettings files

Each wiki has its own LocalSettings-*-private.php file, for passwords etc.
These must be copied into place:

```console
cd /src/wmau-config
cp LocalSettings--private.dist.php LocalSettings-wmauwiki-private.php
cp LocalSettings--private.dist.php LocalSettings-commwiki-private.php
cp LocalSettings--private.dist.php LocalSettings-stardit-private.php
```

## Composer

All extensions are installed with Composer.

To update composer packages,
after updating `composer.local.json` here in this repo and updating the repo on the server as described above,
it's a matter of running:

```console
cd /var/www/mediawiki
sudo -- sudo -u www-data composer update -o --no-dev
```

And then running the [update](https://www.mediawiki.org/wiki/Manual:Update.php) command for each wiki, e.g.:

```console
sudo -- sudo -u www-data ./maintenance/run update --wiki wmauwiki
sudo -- sudo -u www-data ./maintenance/run update --wiki commwiki
sudo -- sudo -u www-data ./maintenance/run update --wiki stardit
```

## Jobs

After setting up the symlinks (see above),
start the [job queue](https://www.mediawiki.org/wiki/Manual:Job_queue#Continuous_service) runners with:

```console
sudo systemctl enable mediawiki-jobs-stardit
sudo systemctl start mediawiki-jobs-stardit
```

Check that they're all enabled and running with:

```console
sudo systemctl list-unit-files | grep mediawiki
```

Inspect their status with:

```console
sudo systemctl status mediawiki-jobs-*
```

## Crontabs

root:

```
MAILTO=tech@wikimedia.org.au
23 0 * * * /srv/wmau-config/backup.sh
45 4 * * * certbot renew --apache
```

www-data:

```
MAILTO=tech@wikimedia.org.au
@daily /usr/bin/php /var/www/mediawiki/maintenance/run initSiteStats --wiki wmauwiki --update --active --quiet
@weekly /usr/bin/php /var/www/mediawiki/maintenance/run generateSitemap --wiki wmauwiki --fspath /var/www/mediawiki/sitemaps --urlpath /mediawiki/sitemaps/ --identifier wikimedia.org.au --server https://wikimedia.org.au --quiet
@weekly /usr/bin/php /var/www/mediawiki/maintenance/run updateSpecialPages --wiki wmauwiki --quiet

@daily /usr/bin/php /var/www/mediawiki/maintenance/run initSiteStats --wiki commwiki --update --active --quiet
@weekly /usr/bin/php /var/www/mediawiki/maintenance/run updateSpecialPages --wiki commwiki --quiet

@daily /usr/bin/php /var/www/mediawiki/maintenance/run initSiteStats --wiki stardit --update --active --quiet
@weekly /usr/bin/php /var/www/mediawiki/maintenance/run generateSitemap --wiki stardit --fspath /var/www/mediawiki/sitemaps --urlpath /mediawiki/sitemaps/ --identifier stardit.wikimedia.org.au --server https://stardit.wikimedia.org.au --quiet
@weekly /usr/bin/php /var/www/mediawiki/maintenance/run updateSpecialPages --wiki stardit --quiet
```

## SMTP

Configure Exim4 as described in https://wiki.debian.org/Exim4Gmail

Logs are at `/var/log/exim4/mainlog`

In addition, MediaWiki is configured to use the same SMTP account, via $wgSMTP.

## Backups

The DBs and page XML (for public wikis) are dumped weekly (see above in *Crontabs*).

Sysadmins download all DB dumps, XML dumps, and `images_*` directories using something like:

```console
rsync --archive -v --itemize-changes --delete --delete-excluded \
  --include='/var/' \
  --include='/var/backups/' \
  --include='/var/backups/wmau/***' \
  --include='/var/www/' \
  --include='/var/www/images_***' \
  --exclude='*' \
  osiris2.wikimedia.org.au:/ $BACKUP_DIR/
```

The XML dumps are uploaded to the Internet Archive.
The `images` directories are are compressed with a command such as:

```console
cd $BACKUP_DIR/var/www/images_wmauwiki
zip ~/tmp/wmau_wmauwiki.zip . --recurse-paths --exclude './thumb/*' --exclude './deleted/*' --exclude './temp/*'
```

Only the database and images directories need to be backed up;
all configuration lives in the `wmau-config` repository.

## Licence

GPL3.0+

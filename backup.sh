#!/bin/bash

BACKUPDIR=/var/backups/wmau

DATABASES=$(mysql -e"SHOW DATABASES")
SKIP="sys Database information_schema performance_schema mysql"

for DB in $DATABASES; do
    for S in $SKIP; do
        if [ $S == $DB ]; then
            continue 2
        fi
    done

    echo "Backing up $DB"
    DBFILE=$BACKUPDIR/$DB.sql.gz
    touch $DBFILE
    chown root:adm $DBFILE
    chmod 0440 $DBFILE
    mysqldump --skip-extended-insert --lock-tables $DB | gzip -c > $DBFILE
done

echo "Dumping XML of wmauwiki"
php /var/www/mediawiki/maintenance/run dumpBackup \
 --full \
 --quiet \
 --wiki wmauwiki \
 --server https://wikimedia.org.au \
 --output=gzip:$BACKUPDIR/wmauwiki-pages.xml.gz
chown root:adm $BACKUPDIR/wmauwiki-pages.xml.gz

echo "Dumping XML of stardit wiki"
php /var/www/mediawiki/maintenance/run dumpBackup \
 --full \
 --quiet \
 --wiki stardit \
 --server https://stardit.wikimedia.org.au \
 --output=gzip:$BACKUPDIR/stardit-pages.xml.gz
chown root:adm $BACKUPDIR/stardit-pages.xml.gz

echo "Backups directory $BACKUPDIR"
ls -lh /var/backups/wmau/

echo "Filesystems:"
df -h

#!/bin/bash

# Wait a minute after the server starts up to give other processes time to get started
echo Starting MediaWiki job service for $WIKI . . .
sleep 60
echo started.

JOBCMD="/usr/bin/php /var/www/mediawiki/maintenance/runJobs.php --wiki=$WIKI"

while true; do

    # Job types that need to be run ASAP no matter how many of them are in the queue
    # Those jobs should be very "cheap" to run
    $JOBCMD --type="enotifNotify"

    # Everything else, limit the number of jobs on each batch
    # The --wait parameter will pause the execution here until new jobs are added,
    # to avoid running the loop without anything to do
    $JOBCMD --wait --maxjobs=20

    # Wait some seconds to let the CPU do other things, like handling web requests, etc
    #echo Waiting for 10 seconds...
    sleep 10

done

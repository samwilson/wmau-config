<?php
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

// See if the --wiki CLI arg has been passed.
$wiki = false;
if ( defined( 'MW_DB' ) ) {
	$wiki = MW_DB;
} elseif ( isset( $_SERVER['SERVER_NAME'] ) ) {
	if ( $_SERVER['SERVER_NAME'] === 'wikimedia.org.au' ) {
		$wiki = 'wmauwiki';
	} elseif ($_SERVER['SERVER_NAME'] === 'comm.wikimedia.org.au') {
		$wiki = 'commwiki';
	} elseif ($_SERVER['SERVER_NAME'] === 'stardit.wikimedia.org.au') {
		$wiki = 'stardit';
	}
}
// Per-wiki LocalSettings.php files are included at the end of this file.
if ( !file_exists( __DIR__ . "/LocalSettings-$wiki.php" ) ) {
	die( "Wiki not installed on this server: $wiki\n" );
}

$wgScriptPath = '/w';
$wgArticlePath = '/wiki/$1';
$wgMainPageIsDomainRoot = true;

$wgLogos = [
	'icon' => "https://wikimedia.org.au/w/images/1/14/Logo-black-small.png",
	'wordmark' => [
		'src' => "https://wikimedia.org.au/w/images/b/bb/Wmau-wordmark.png",
		'width' => 200,
		'height' => 57,
	],
];

$wgLanguageCode = 'en';
$wgRestrictDisplayTitle = false;
$wgActions['mcrundo'] = false;
$wgActions['mcrrestore'] = false;
$wgWhitelistRead = [];
$wgWhitelistReadRegexp = [];

## Jobs are run via systemd services.
$wgJobRunRate = 0;

## Uploads and images.
$wgUploadDirectory = '/var/www/images_' . $wiki;
$wgUploadPath = "/w/images";
$wgEnableUploads = true;
$wgMaxUploadSize = 1024 * 1024 * 100;
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";
$wgUseInstantCommons = true;
$wgGenerateThumbnailOnParse = true;
$wgThumbnailScriptPath = "/w/thumb.php";
$wgAttemptFailureEpoch = 2;
$wgGalleryOptions['mode'] = 'packed';

## Database.
$wgDBtype = 'mysql';
$wgDBserver = 'localhost';
$wgDBprefix = '';
$wgDBTableOptions = 'ENGINE=InnoDB, DEFAULT CHARSET=binary';

## Skins.
wfLoadSkins( [ 'Vector', 'WMAU' ] );
$wgDefaultSkin = 'Vector';
$wgAllowUserCss = true;
$wgAllowUserJs = true;
$wgEnableCreativeCommonsRdf = true;
$wgRightsPage = '';
$wgRightsUrl = "http://creativecommons.org/licenses/by-sa/3.0/";
$wgRightsText = "Attribution-ShareAlike 3.0 Unported";
$wgRightsIcon = "$wgScriptPath/resources/assets/licenses/cc-by-sa.png";

# Caching.
$wgCacheEpoch = 20160130232602;
$wgMainCacheType = CACHE_ACCEL;
$wgSessionCacheType = CACHE_DB;
$wgMiserMode = true;

## Extensions.

wfLoadExtension( 'PdfHandler' );
$wgPdfProcessor = '/usr/bin/gs';
$wgPdfPostProcessor = '/usr/bin/convert';
$wgPdfInfo = '/usr/bin/pdfinfo';
$wgPdftoText = '/usr/bin/pdftotext';

wfLoadExtension( 'WikiSEO' );
$wgTwitterSiteHandle = '@wm_au';
$wgTwitterCardType = 'summary';

wfLoadExtension( 'ParserFunctions' );
$wgPFEnableStringFunctions = true;

wfLoadExtension( 'Scribunto' );
$wgScribuntoDefaultEngine = 'luastandalone';
$wgScribuntoUseGeSHi = true;
$wgScribuntoUseCodeEditor = true;

wfLoadExtension( 'CodeMirror' );
wfLoadExtension( 'CodeEditor' );
$wgDefaultUserOptions['usebetatoolbar'] = 1;

wfLoadExtension( 'Echo' );
$wgDefaultUserOptions["echo-subscriptions-email-mention"] = true;
$wgDefaultUserOptions["echo-subscriptions-email-edit-thank"] = true;

wfLoadExtension( 'SyntaxHighlight_GeSHi' );

wfLoadExtension( 'Thanks' );

wfLoadExtension('WikiEditor');
$wgDefaultUserOptions['usebetatoolbar'] = 1;
$wgWikiEditorRealtimePreview = true;

wfLoadExtension('VisualEditor');

wfLoadExtension( 'MsUpload' );
$wgMSU_useDragDrop = true;

wfLoadExtension('PageForms');

wfLoadExtension( 'Cargo' );
$wgCargoAllowedSQLFunctions[] = 'NOW';
$wgCargoAllowedSQLFunctions[] = 'ADDDATE';
$wgCargoAllowedSQLFunctions[] = 'IFNULL';
$wgCargoAllowedSQLFunctions[] = 'COALESCE';
$wgCargoDefaultStringBytes = 191;
$wgGroupPermissions['*']['runcargoqueries'] = false;
$wgGroupPermissions['sysop']['runcargoqueries'] = true;

wfLoadExtension( 'InputBox' );

wfLoadExtension( 'Cite' );

wfLoadExtension( 'TemplateData' );

wfLoadExtension( 'TemplateWizard' );

wfLoadExtension( 'TemplateStyles' );

wfLoadExtension( 'WikidataPageBanner' );
$wgWPBNamespaces = true;
$wgWPBDisplaySubtitleAfterBannerSkins = ['wmau'];
$wgWPBEnableHeadingOverride = false;
$wgWPBEnableMainPage = true;

wfLoadExtension( 'LinkCards' );

wfLoadExtension( 'Interwiki' );
$wgGroupPermissions['*']['interwiki'] = false;
$wgGroupPermissions['sysop']['interwiki'] = true;

wfLoadExtension( 'TimezoneConverter' );

wfLoadExtension( 'TimedMediaHandler' );

wfLoadExtension( 'Renameuser' );

wfLoadExtension( 'UnlinkedWikibase' );

wfLoadExtension( 'TitleKey' );

# Load site-specific config options and private variables.
require_once __DIR__ . '/LocalSettings-' . $wiki . '.php';
require_once __DIR__ . '/LocalSettings-' . $wiki . '-private.php';

// Lastly, define any config that depends on what's defined in the per-wiki files.

// Email.
$wgEnableEmail = true;
$wgEnableUserEmail  = true;
$wgEmergencyContact = "tech@wikimedia.org.au";
$wgPasswordSender = "tech@wikimedia.org.au";
$wgEnotifUserTalk = true;
$wgEnotifWatchlist = true;
$wgEmailAuthentication = true;
$wgSMTP = [
	'host' => 'ssl://smtp.gmail.com',
	'IDHost' => 'gmail.com',
	'localhost' => 'localhost',
	'port' => 465,
	'username' => $wmauSMTPUsername,
	'password' => $wmauSMTPPassword,
	'auth' => true
];

// OAuth user authentication.
if ( isset( $wmauOAuthClientId ) && $wmauOAuthClientId ) {
	wfLoadExtensions( [ 'PluggableAuth', 'WSOAuth' ] );
	$wgPluggableAuth_EnableLocalLogin = true;
	$wgPluggableAuth_Config['Wikimedia'] = [
		'plugin' => 'WSOAuth',
		'buttonLabelMessage' => 'login-with-wikimedia',
		'data' => [ 
			'type' => 'mediawiki',
			'uri' => 'https://meta.wikimedia.org/w/index.php?title=Special:OAuth',
			// oauth 1.0a
			'clientId' => $wmauOAuthClientId,
			'clientSecret' => $wmauOAuthClientSecret,
		],
	];
}
